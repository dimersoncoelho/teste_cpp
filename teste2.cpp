#include <iostream>
using namespace std;

int **soma_vet(int** vet, int linha, int coluna)
{
	for (int i = 0; i < linha; i++)
	{
		for (int j = 0; j < coluna; j++)
		{
			vet[i][j] = vet[i][j] + 1;
		}
	}

	return vet;
}

int main()
{
	int coluna, linha, i, j;

	cin >> linha;
	cin >> coluna;

	int **vet = new int*[linha];
	for (i = 0; i < linha; i++)
	{
		vet[i] = new int[coluna];
		for(j = 0; j < coluna; j++)
		{
			cin >> vet[i][j];
		}
	}

	vet = soma_vet(vet, linha, coluna);

	for(i = 0; i < linha; i++)
	{
		for (j = 0; j < coluna; j++)
		{
			cout << vet[i][j] << " ";
		}
		cout << endl;
	}


	system("pause");
	return 0;
}