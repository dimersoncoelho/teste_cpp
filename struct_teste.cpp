#include <iostream>

using namespace std;

struct Pneu
{
	int aroPneu;
	int precoPneu;
};

struct Motor
{
	int potenciaMotor;
	double precoMotor;
};

struct Carro
{
	string marca;
	int ano;
	double valor;
	//Motor motor;
	//Pneu pneu;
};

struct Lista
{
	Carro *carro;
	Lista *next;
};

Lista* aloca(Lista *lista, Carro *carro)
{
	if (lista == NULL)
	{
		Lista *aux = new Lista;
		aux->carro = carro;
		aux->next = NULL;
		return aux;
	}

	lista->next = aloca(lista->next,carro);
	return lista;
};

Carro* aloca_carro(string s, int year, double value)
{
	Carro *novo = new Carro;

	novo->marca = s;
	novo->ano = year;
	novo->valor = value;

	return novo;
};

void altera_valor(Carro *carro, int valor)
{
	carro->valor = valor;
}

int main()
{
	Carro *aux1;
	Carro *aux2;
	Carro *aux3;

	aux1 = aloca_carro("Celta", 2012, 30000);
	altera_valor(aux1, 10000);
	aux2 = aloca_carro("Ferrari", 2021, 1500000);
	aux3 = aloca_carro("HB20", 2018, 400000);

	Lista *lista = NULL;

	lista = aloca(lista,aux1);
	lista = aloca(lista,aux2);
	lista = aloca(lista,aux3);

	while(lista != NULL)
	{
		cout << lista->carro->marca << " " << lista->carro->ano << " " << lista->carro->valor << endl;
		lista = lista->next;
	}

	return 0;
}