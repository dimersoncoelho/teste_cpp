#include <iostream>
#include <sstream>

int main()
{
	string s;
	stringstream ss;
	int x;

	getline(cin, s);

	ss << s;

	while(ss >> x)
	{
		cout << x*10 << endl;
	}

	return 0;
}