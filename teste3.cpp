#include <iostream>
using namespace std;

int** fillPaint(int **arr, int SIZE, int i, int j, int color) {
    int a = i + 1;
    int b = j + 1;
    arr[i][j] = color;
    bool limite_linha = (a == 1 || a == SIZE);
    bool limite_coluna = (b == 1 || b == SIZE);

    //cout << a << " " << b << endl;
  
    if (not limite_linha)
    {
      if(arr[i-1][j] == 0)
        fillPaint(arr, SIZE, i-1, j, color);
      if(arr[i+1][j] == 0)
        fillPaint(arr, SIZE, i+1, j, color);
    }
  
    if (not limite_coluna)
    {
      if(arr[i][j-1] == 0)
        fillPaint(arr, SIZE, i, j-1, color);
      if(arr[i][j+1] == 0)
        fillPaint(arr, SIZE, i, j+1, color);
    }

    for (i=0; i<SIZE; i++) {
        for (j=0; j<SIZE; j++) {
            cout << arr[i][j] << " ";
        }
        cout << endl;
    }
    cout << endl;
  
    return 0;
}

int main() {
    int SIZE, i, j, color;
    cin >> SIZE;
    int **arr = new int*[SIZE];
    for (i=0; i<SIZE; i++) {
        arr[i] = new int[SIZE];
        for (j=0; j<SIZE; j++) {
            cin >> arr[i][j];
        }
    }
    cin >> i >> j;
    cin >> color;
    arr = fillPaint(arr,SIZE-1,i,j,color);
    for (i=0; i<SIZE; i++) {
        for (j=0; j<SIZE; j++) {
            cout << arr[i][j] << " ";
        }
        cout << endl;
    }
    return 0;
}
