#include <iostream>

using namespace std;

void fun(int a[], int n)
{
	for (int i = 0; i < n; i++)
		a[i] = a[i]+1;
}

int main()
{

	int notas[10] = {1,2,4,5};


	for (int i = 0; i < 10; i++)
		notas[i] = i;

	fun(notas,10);

	cout << notas[2] << notas[6] << notas[8] << "\t" << notas << "\n";

	return 0;
}