#ifndef PESSOA_H
#define PESSOA_H

#include <string>
using namespace std;

class Pessoa{
public:
	int idade;
	string nome;
	void seta_sexo(char sexo);
	char revela_sexo();

private:
	char sexo;
};

#endif